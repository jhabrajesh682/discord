const image = require('../model/multipleImageupload.model');
const fs = require('fs');
const path = require('path');


class images {


    async uploadImage(req, res) {
        console.log(req.files.image)
        let date = new Date();
        let timestamp = date.getTime();
        let imageName
        let imageArray = []

        let imagesFile = req.files.image;
        if (Array.isArray(imagesFile) === false) {

            imageName = timestamp + req.files.image.name + '.jpg';
            let time = 'image/' + timestamp + req.files.image.name + '.jpg';
            imagesFile.mv(time)
            imageArray.push(imageName)
        }
        else if (Array.isArray(imagesFile) === true) {
            for (const iterator of imagesFile) {

                let MulImageName = timestamp + iterator.name + '.jpg';
                let time = 'image/' + timestamp + iterator.name + '.jpg';
                iterator.mv(time)
                imageArray.push(MulImageName)
            }
        }

        let saveImage = new image({
            image: imageArray,
            imageInfo: req.body.imageInfo
        })
        await saveImage.save()

        return res.status(200).send({
            status: true,
            message: 'image successfully saved'
        })
    }



}
module.exports = images