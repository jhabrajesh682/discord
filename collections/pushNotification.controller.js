const pushNotification = require('../model/pushNotification.model')
var gcm = require('node-gcm');

class push {

    //this function send notification and save the notification into database
    async sendNotification(req, res) {

        try {
            let registrationToken = []

            //firebase device token 
            let fireTokens = '326C080E1DEEB235'
            registrationToken.push(fireTokens)

            let notificationSaved = await pushNotification({

                title: req.body.title,
                icon: req.body.icon,
                body: req.body.body,
            })

            await notificationSaved.save();

            const message = new gcm.Message({
                data: {
                    key1: "message1",
                    key2: "message2",
                },
                notification: {
                    title: req.body.title,
                    icon: req.body.icon,
                    body: req.body.body
                },
            });

            const sender = new gcm.Sender(process.env.firebaseServerKey);
            sender.send(message, { registrationTokens: registrationToken }, async function (err, response) {
                if (err) {

                    return res.send(err);
                }
                else {

                    return res.status(200).send({
                        status: true,
                        data: response
                    })
                }
            });

        } catch (error) {

        }
    }

    async getAllNotifications(req, res) {

        let limit
        let page
        if (req.query.limit) {
            limit = (parseInt(req.query.limit) ? parseInt(req.query.limit) : 10);
            page = req.query.page ? (parseInt(req.query.page) ? parseInt(req.query.page) : 1) : 1;
        }

        let data = await pushNotification.find({}).
            sort({ _id: -1 })
            .limit(limit)
            .lean()

        return res.status(200).send({
            status: true,
            result: data
        })

    }

    async getOneNotificationAndUpdate(req, res) {

        let notficationData = await pushNotification.findById(req.params.id);
        if (!notficationData || !req.params.id) {
            return res.status(404).send({
                status: false,
                message: 'Not found'
            })
        }

        await pushNotification.updateOne({ _id: req.params.id }, {
            $set: {
                title: req.body.title,
                body: req.body.body,
                icon: req.body.icon
            }
        })

        return res.status(200).send({
            status: true,
            message: 'successfully updated'
        })
    }

    async getOneNotificationAndDelete(req, res) {

        let notficationDataExist = await pushNotification.findById(req.params.id);
        if (!notficationDataExist || !req.params.id) {
            return res.status(404).send({
                status: false,
                message: 'Not found'
            })
        }
        await pushNotification.findByIdAndRemove(req.params.id)

        return res.status(200).send({
            status: true,
            message: 'successfully deleted'
        })


    }
}

module.exports = push