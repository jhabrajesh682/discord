const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pushNotification = new Schema({


    title: {
        type: String,
        required: true
    },

    body: {
        type: String,
        required: true
    },

    icon: {
        type: String
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('notification', pushNotification)