const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const image = new Schema({


    image: {
        type: Array,
        required: true
    },

    imageInfo: {
        type: String,
        required: true
    },

}, {
    timestamps: true
})

module.exports = mongoose.model('image', image)