const express = require('express');
const app = express();
require('dotenv').config()
var bodyParser = require('body-parser')
app.use(bodyParser.json())
const discords = require('./startup/discord');

app.get('/index', (req, res) => {
    res.sendFile(__dirname + '/index.html');
})

app.listen(process.env.port, () => {
    console.log(`server started at port ${process.env.port}`)
    discords()
})