const {Client, Intents} = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.DIRECT_MESSAGES] , partials: [
  'CHANNEL', // Required to receive DMs
]});
const userArray = []
const {
  getSheetReady, 
  getRows,
  findEmailId,
  updateDetails,
  getChannelAndRoleId
} = require('../startup/sheet');  // all the google sheet function are here.
const {clearTimeouts} = require('../startup/timeout')
const prefix = '!join-'

module.exports = async function () {
  client.on('ready', () => {
      console.log(`${client.user.username} has started working`)
  })

  client.on('messageCreate', async (message) => {
    await getSheetReady(); // getting the google sheet ready
    if (message.author.bot) return false
    
    if(message.content.startsWith(prefix)){
      
        const cmdName = message.content.substring(prefix.length) // checking the command is correct or not
  
        let sheetData = await getRows(1)
          const isClanExist = sheetData.findIndex(x => x.clan === cmdName && x.status === 'yes'); // checking is clan exist if exist then it active or not
          if (isClanExist > -1) {
            const timeout = setTimeout(() => {
              message.author.send('You have timed out.') // 5 min timeout function
            }, process.env.timeout);
            userArray.push({      // keeping the record of timer and user id in an array for 
              timeoutId: timeout, //large scale we have to use Redis currently i have saved in an array.
              userId: message.author.id,
              startTime: new Date().getTime(),
              clanName: cmdName
            })
            await message.reply(`Please check your DM to verify your Id?`)
            await message.author.send('Please Provide your EmailId')
          } else {
            message.reply(`Sorry this Clan is not active now.`)
          }
    } else {

      await clearTimeouts(userArray.find(x => x.userId === message.author.id))
      const index = userArray.findIndex(x => x.userId === message.author.id)

      const regex = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');
      const status = regex.test(message.content) // checking is the incoming msg email or not.
      if (status) {
        message.author.send(`Please give me a time i am checking your details.`)
        const emailStatus = await findEmailId(0, message.content)

        if (emailStatus) {
          const getDetails = await getChannelAndRoleId(1, userArray[index].clanName)
          await updateDetails(0, message.content, getDetails.channelId, getDetails.roleId)
          const channel = client.channels.cache.find(channel => channel.id === getDetails.channelId)
          channel.send(`welcome to the clan @${message.author.username}`)
          message.author.send(`Welcome to the clan 🚀 ${userArray[index].clanName}. now you can move to clan start discussion there.`)
        } else if(!emailStatus) {
          message.author.send('Sorry this email is not registered with us. Please enter a valid email address. Type the command again in bot channel to start verification process again.')
        }
      } else {
        message.author.send(`Please Enter a valid email Id`)
      }
      userArray.splice(index, 1);
    }

  })

  client.login(process.env.discordJS_BOT_KEY)
}

