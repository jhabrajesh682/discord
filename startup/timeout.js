

async function clearTimeouts(timeout) {
  if (timeout && timeout !=='undefined' && timeout != null) {
    let dif = (new Date().getTime() - timeout.startTime);
    dif = Math.round((dif/1000)/60);
    if (dif <= 1) {
      clearTimeout(timeout.timeoutId);
    }    
  }
  return
}

module.exports = {
  clearTimeouts
}