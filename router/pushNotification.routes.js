const router = require("express").Router();
const push = require('../collections/pushNotification.controller');

const pushes = new push();

router.post('/sendNotification', pushes.sendNotification)

router.get('/getAllNotifications', pushes.getAllNotifications)

router.put('/updateNotifications/:id', pushes.getOneNotificationAndUpdate);

router.delete('/deleteNotifications/:id', pushes.getOneNotificationAndDelete)


module.exports = router